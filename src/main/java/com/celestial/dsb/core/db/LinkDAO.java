package com.celestial.dsb.core.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import domain.Bookmark;
import java.sql.CallableStatement;

public class LinkDAO
{

    Connection con = null;

    private static String linkTableName = "link";
    private static String joiningTable = "linkToMetatag";
    private static String tagTableName = "metatag";

    public void saveLink(String name, String description, ArrayList<String> tags) throws SQLException
    {
        PreparedStatement saveStmt = null;
        // SQL statement to insert into the link table
        String linkSql = "insert into " + linkTableName + " (link_name, link_description) values( ?,?)";
        try
        {
        	con = DBConnector.getConnector().getConnection();
            saveStmt = con.prepareStatement(linkSql);
            saveStmt.setString(1, name);
            saveStmt.setString(2, description);
            saveStmt.executeUpdate();
            
            // Setting AutoCommit to false means we can execute multiple SQL
            // statememts in our code in a single executeUpdate() call
            con.setAutoCommit(false);
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            
            // This next line creates a call to a stored procedure
            CallableStatement cStmt = con.prepareCall("{call saveTag(?)}");
            int linkId = getCurrentIDNum() + 1;
            
            // Iterate over all the tags
            for (String tag : tags)
            {
                String query = "insert into linkToMetatag (tag_name, link_id) values ('" + tag + "', " + linkId + " )";

                stmt.addBatch(query);
                
                cStmt.setString(1, tag);
                

                cStmt.addBatch();   
            }
            
            saveStmt.executeUpdate();
            
            stmt.executeBatch();
            cStmt.executeBatch();

 
            con.commit();
            stmt.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int getCurrentIDNum()
    {
        int result = 0;
        try
        {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select link_id from " + linkTableName);
            while (rs.next())
            {
                int idNum = rs.getInt("link_id");
                if (idNum > result)
                {
                    result = idNum;
                }
            }
        } catch (SQLException se)
        {
            System.out.println(se);
        }
        return result;
    }

    public ArrayList<Bookmark> getAllLinks()
    {
        PreparedStatement selectStmt = null;
        String sql =  "select * from " + linkTableName;
        ResultSet result;
        
        ArrayList<Bookmark> bookmarks = new ArrayList<>();
        try
        {
            Bookmark bookmark;
            con = DBConnector.getConnector().getConnection();
            selectStmt = con.prepareStatement(sql);
            result = selectStmt.executeQuery();
            while (result.next())
            {
            	bookmark = new Bookmark( result.getInt(1), result.getString(2), result.getString(3));
            	bookmarks.add(bookmark);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            bookmarks = null;
        } 
        return bookmarks;
    }

    public ArrayList<Bookmark> getLinksForTag(String metatag)
    {
    	Bookmark bookmark;
        String sql = "SELECT * FROM " + linkTableName + " INNER JOIN " + joiningTable + " ON " + joiningTable + ".link_id = " + linkTableName
                + ".link_id where " + joiningTable + ".tag_name = ?";
        ResultSet result;
        PreparedStatement selectStmt = null;
        ArrayList<Bookmark> bookmarks = new ArrayList<>();
        try
        {
            con = DBConnector.getConnector().getConnection();
            selectStmt = con.prepareStatement(sql);
            selectStmt.setString(1, metatag);
            result = selectStmt.executeQuery();
            while (result.next())
            {
            	bookmark = new Bookmark(result.getInt(1), result.getString(2), result.getString(3));
                bookmarks.add(bookmark);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            bookmarks = null;
        } 
        return bookmarks;
    }
}
