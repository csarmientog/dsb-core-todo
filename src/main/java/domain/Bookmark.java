package domain;

import java.util.ArrayList;

public class Bookmark {
	private String url;
	private String description;
	private ArrayList<String> tags;
	private int id;
	
	public Bookmark( int id, String url, String description ){
		this.id = id;
		this.url = url;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getUrl() {
		return url;
	}

	public String getDescription() {
		return description;
	}
	
	
}
